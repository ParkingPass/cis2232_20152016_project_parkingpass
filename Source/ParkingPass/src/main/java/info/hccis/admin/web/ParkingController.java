package info.hccis.admin.web;
import info.hccis.admin.data.springdatajpa.InfractionRepository;
import info.hccis.admin.data.springdatajpa.PassesRepository;
import info.hccis.admin.model.jpa.Infractions;
import info.hccis.admin.model.jpa.Passes;
import info.hccis.admin.model.jpa.User;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * Created by compa on 2015-12-05.
 */
@Controller
public class ParkingController {
    private final PassesRepository pr;
    private final InfractionRepository ir;
    private String login;

    @Autowired
    public ParkingController(PassesRepository parkRepo, InfractionRepository infRepo) {
        this.ir = infRepo;
        this.pr = parkRepo;
    }

    @RequestMapping("/parking/pass_application")
    public String showPassApplication(Model model, HttpServletRequest request, HttpSession session) {
        try {
            HttpSession sessionData = request.getSession();

            if (sessionData.getAttribute("userData") != null) {
                Passes pass = new Passes();
                model.addAttribute("passes", pass);
                return "parking/pass_application";
            } else {
                return "usertools/pleaseLogin";
            }
        } catch (Exception ex){
            model.addAttribute("message", ex.getMessage());
            return "other/error";
        }
    }

    @RequestMapping(value = "/parking/process_application", method = RequestMethod.POST)
    public String processPassApplication(Model model, HttpServletRequest request, HttpSession session, @Valid @ModelAttribute("passes") Passes pass) {
        try {
            HttpSession sessionData = request.getSession();

            if (sessionData.getAttribute("userData") != null) {
                pr.save(pass);
                model.addAttribute("title", "Pass Added");
                model.addAttribute("message", "Pass was successfully added!");
                return "/other/generalMessage";
            } else {
                return "usertools/pleaseLogin";
            }
        } catch (Exception ex){
            model.addAttribute("message", ex.getMessage());
            return "other/error";
        }
    }

    @RequestMapping("/parking/view_pass_details")
    public String showPassDetails(Model model, HttpServletRequest request, HttpSession session, @RequestParam("passid") Integer passId) {
        try {
            HttpSession sessionData = request.getSession();

            if (sessionData.getAttribute("userData") != null) {
                User userData = (User) sessionData.getAttribute("userData");

                Passes passData = pr.findOne(passId);
                List<Infractions> infData = ir.findByPassId(passId);
                model.addAttribute("pass", passData);
                model.addAttribute("infraction", infData);
                return "parking/detailedPass";
            } else {
                return "usertools/pleaseLogin";
            }
        } catch (Exception ex){
            model.addAttribute("message", ex.getMessage());
            return "other/error";
        }
    }

    @RequestMapping("/parking/pass_overview")
    public String showAllPasses(Model model, HttpServletRequest request, HttpSession session) {
        try {
            HttpSession sessionData = request.getSession();

            if (sessionData.getAttribute("userData") != null) {
                User userData = (User) sessionData.getAttribute("userData");

                List<Passes> passes = pr.findByHolderId(userData.getId());
                model.addAttribute("passes", passes);
                return "parking/pass_overview";
            } else {
                return "usertools/pleaseLogin";
            }
        } catch (Exception ex){
            model.addAttribute("message", ex.getMessage());
            return "other/error";
        }
    }

    @RequestMapping("/parking/infractions")
    public String showInfractions(Model model, HttpServletRequest request, HttpSession session) {
        try {
            HttpSession sessionData = request.getSession();

            if (sessionData.getAttribute("userData") != null) {
                User userData = (User) sessionData.getAttribute("userData");

                List<Infractions> infractions = ir.findByUserId(userData.getId());
                model.addAttribute("infractions", infractions);
                return "parking/infraction_overview";
            } else {
                return "usertools/pleaseLogin";
            }
        } catch (Exception ex){
            model.addAttribute("message", ex.getMessage());
            return "other/error";
        }
    }
}
