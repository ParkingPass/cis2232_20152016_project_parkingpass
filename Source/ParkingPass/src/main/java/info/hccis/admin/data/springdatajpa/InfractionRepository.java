package info.hccis.admin.data.springdatajpa;

import com.sun.xml.ws.api.tx.at.Transactional;
import info.hccis.admin.model.jpa.CodeValue;
import info.hccis.admin.model.jpa.Infractions;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by compa on 2015-12-13.
 */
@Repository
public interface InfractionRepository extends CrudRepository<Infractions, Integer> {
    Infractions findOne(Integer id);
    List<Infractions> findAll();
    List<Infractions> findByUserId(Integer UserId);
    List<Infractions> findByPassId(Integer PassId);
}
