package info.hccis.admin.model.jpa;

import org.joda.time.DateTime;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * Created by compa on 2015-12-15.
 */
@Entity
@Table(name = "useraccess")
public class User {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "userAccessId")
    private Integer id;

    @Basic(optional = false)
    @NotNull
    @Size(min = 3, max = 15)
    @Column(name = "username")
    private String userName;

    @Basic(optional = false)
    @NotNull
    @Size(min = 5)
    @Column(name = "password")
    private String passWord;

    @Basic(optional = false)
    @NotNull
    @Column(name = "userTypeCode")
    private Integer userTypeCode = 11;

    @Basic(optional = false)
    @NotNull
    @Column(name = "createdDateTime")
    @Temporal(TemporalType.TIMESTAMP)
    private java.util.Date createdTime;



    public User() {
    }

    public User(Integer passID) {
        this.id = passID;
    }

    public Integer getId() {
        return id;
    }
    public void setId(Integer passID) { this.id = passID; }

    public String getUserName() { return userName;}
    public void setUserName(String name) {
        this.userName = name;
    }

    public String getPassWord() { return passWord;}
    public void setPassWord(String pass) { this.passWord = pass; }

    public Integer getUserTypeCode() {
        return userTypeCode;
    }
    public void setUserTypeCode(Integer type) {
        this.userTypeCode = type;
    }

    public java.util.Date getCreatedTime() {
        return createdTime;
    }
    public void setCreatedTime(java.util.Date time) {
        this.createdTime = time;
    }

}
