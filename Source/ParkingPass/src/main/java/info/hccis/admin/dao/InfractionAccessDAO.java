package info.hccis.admin.dao;

import info.hccis.admin.dao.util.ConnectionUtils;
import info.hccis.admin.dao.util.DbUtils;
import info.hccis.admin.model.DatabaseConnection;
import info.hccis.admin.model.jpa.Infractions;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;

/**
 * Created by compa on 2015-12-13.
 */
public class InfractionAccessDAO {
    public static ArrayList<Infractions> getInfractions(DatabaseConnection databaseConnection) {
        ArrayList<Infractions> infractions = new ArrayList();

        PreparedStatement ps = null;
        String sql = null;
        Connection conn = null;

        try {
            conn = ConnectionUtils.getConnection(databaseConnection);

            sql = "SELECT * FROM infractions" ;

            ps = conn.prepareStatement(sql);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                // It is possible to get the columns via name
                // also possible to get the columns via the column number
                // which starts at 1

                // e.g. resultSet.getString(2);

                Infractions aClass = new Infractions();
                aClass.setId(rs.getInt("id"));
                aClass.setPassId(rs.getInt("pass_id"));
                aClass.setUserId(rs.getInt("user_id"));
                aClass.setInfTitle(rs.getString("inf_title"));
                aClass.setInfDescription(rs.getString("inf_description"));
                aClass.setInfDate(rs.getString("inf_date"));

                System.out.println("Found infraction type=" + aClass);
                infractions.add(aClass);
            }
        } catch (Exception e) {
            String errorMessage = e.getMessage();
            e.printStackTrace();
        } finally {
            DbUtils.close(ps, conn);
        }

        return infractions;

    }

    /*public static void add(DatabaseConnection databaseConnection, Assignment classToAdd) {
        PreparedStatement ps = null;
        String sql = null;
        Connection conn = null;

        try {
            conn = ConnectionUtils.getConnection(databaseConnection);

            sql = "INSERT INTO assignments (id, "
                    + "assignmentName, assignmentWeight)"
                    + " VALUES (NULL, '"  + classToAdd.getAssignmentName()+ "', " + classToAdd.getAssignmentWeight() + ")";
            System.out.println("\n\n\n" + sql.toString());

            ps = conn.prepareStatement(sql);
            ps.executeUpdate();

        } catch (SQLException ex) {
            Logger.getLogger(assignmentsDAO.class.getName()).log(Level.SEVERE, null, ex);
        } catch (Exception e) {
            String errorMessage = e.getMessage();
            e.printStackTrace();
        } finally {
            DbUtils.close(ps, conn);
        }
    }*/
}
