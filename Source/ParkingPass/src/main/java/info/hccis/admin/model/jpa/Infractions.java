package info.hccis.admin.model.jpa;

import java.time.Year;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Entity
@Table(name = "infractions")
public class Infractions {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;

    @Basic(optional = false)
    @Column(name = "pass_id")
    private Integer passId;


    @Basic(optional = false)
    @Column(name = "user_id")
    private Integer userId;

    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 150)
    @Column(name = "inf_title")
    private String infTitle;

    @Basic(optional = false)
    @NotNull
    @Size(min = 1)
    @Column(name = "inf_description")
    private String infDescription;

    @Basic(optional = false)
    @Column(name = "inf_date")
    private String infDate;


    public Infractions() {
    }

    public Infractions(Integer infID) {
        this.id = infID;
    }

    public Integer getId() {
        return id;
    }
    public void setId(Integer infId) {
        this.id = infId;
    }

    public Integer getPassId() {
        return passId;
    }
    public void setPassId(Integer num) {
        this.passId = num;
    }

    public Integer getUserId() {
        return userId;
    }
    public void setUserId(Integer num) {
        this.userId = num;
    }

    public String getInfTitle() { return infTitle; }
    public void setInfTitle(String text) {
        this.infTitle = text;
    }

    public String getInfDescription() {
        return infDescription;
    }
    public void setInfDescription(String text) {
        this.infDescription = text;
    }

    public String getInfDate() { return infDate;}
    public void setInfDate(String date) {
        this.infDate = date;
    }

}
