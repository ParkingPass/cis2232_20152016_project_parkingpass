package info.hccis.admin.data.springdatajpa;

import info.hccis.admin.model.jpa.User;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by compa on 2015-12-15.
 */
@Repository
public interface UsersRepository extends CrudRepository<User, Integer> {
    User findOne(Integer id);
    User findOneByUserName(String username);
    List<User> findAll();
}
