package info.hccis.admin.web.services;

import info.hccis.admin.dao.UserAccessDAO;
import info.hccis.admin.model.DatabaseConnection;
import info.hccis.admin.model.entity.Users;
import info.hccis.admin.util.Utility;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.core.Response;

@Path("/useraccess")
public class UserAccessRest {

    @GET
    @Path("/{param}")
    public Response getMsg(@PathParam("param") String msg) {
        int codeType = 0;
        try {
            String[] theParts = msg.split(",");
            String database = theParts[0];
            String username = theParts[1];
            String password = theParts[2];
            try {
                codeType = Integer.parseInt(UserAccessDAO.getUserTypeCode(database, username, password));
            } catch (Exception e) {
                System.out.println("Error: " + e);
            }

        } catch (Exception e) {
            System.out.println("In access web service.  There was an error with the input.");
        }

        return Response.status(200).entity(String.valueOf(codeType)).build();

    }

    @GET
    @Path("/getPasswordHash/{param}")
    public Response getPasswordHash(@PathParam("param") String passwordIn) {

//                boolean valid = true;
        return Response.status(200).entity(Utility.getHashPassword(passwordIn)).build();

    }
    //This one needs both the connection and user object to be passed into it
     @GET
    @Path("/add/{param}")
    public Response addUser(@PathParam("param") String msg) {
        int codeType = 0;
         String database;
         Users user = new Users();
        try {
            String[] theParts = msg.split(",");
            database = theParts[0];
            user.setUsername(theParts[1]);
            user.setPassword(theParts[2]);
            user.setUserTypeCode(Integer.parseInt(theParts[3]));
            codeType = 1;

            try {
                UserAccessDAO.insert(database, user);
            } catch (Exception e) {
                System.out.println("Error: " + e);
            }

        } catch (Exception e) {
            System.out.println("In access web service.  There was an error with the input.");
        }

        return Response.status(200).entity(String.valueOf(codeType)).build();

    }
    
    @GET
    @Path("/update/{param}")
    public Response updateUser(@PathParam("param") String msg) {
        int codeType = 0;
         DatabaseConnection database = new DatabaseConnection();
         Users user = new Users();
        try {
            String[] theParts = msg.split(",");
            database.setDatabaseName(theParts[0]);
            database.setUserName(theParts[1]);
            database.setPassword(theParts[2]);

            user.setUserAccessId(Integer.parseInt(theParts[3]));
            user.setUsername(theParts[4]);
            user.setPassword(theParts[5]);
            user.setUserTypeCode(Integer.parseInt(theParts[6]));

            try {
                UserAccessDAO.update(database, user);
            } catch (Exception e) {
                System.out.println("Error: " + e);
            }

        } catch (Exception e) {
            System.out.println("In access web service.  There was an error with the input:" + e.getMessage());
        }

        return Response.status(200).entity(String.valueOf(codeType)).build();

    }
    
    @GET
    @Path("/delete/{param}")
    public Response deleteUser(@PathParam("param") String msg) {
        int codeType = 0;
         DatabaseConnection database = new DatabaseConnection();
         int userId = 0;
        try {
            String[] theParts = msg.split(",");
            database.setDatabaseName(theParts[0]);
            database.setUserName(theParts[1]);
            database.setPassword(theParts[2]);
            
            userId =Integer.parseInt(theParts[3]);

            try {
                UserAccessDAO.delete(database, userId);
            } catch (Exception e) {
                System.out.println("Error: " + e);
            }

        } catch (Exception e) {
            System.out.println("In access web service.  There was an error with the input.");
        }

        return Response.status(200).entity(String.valueOf(codeType)).build();

    }

}
