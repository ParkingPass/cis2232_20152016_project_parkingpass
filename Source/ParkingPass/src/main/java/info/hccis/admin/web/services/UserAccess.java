/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package info.hccis.admin.web.services;

import info.hccis.admin.dao.UserAccessDAO;
import info.hccis.admin.model.DatabaseConnection;
import info.hccis.admin.model.entity.Users;
import info.hccis.admin.util.Utility;
import javax.jws.WebService;
import javax.jws.WebMethod;
import javax.jws.WebParam;

/**
 *
 * @author bjmaclean
 */
@WebService(serviceName = "UserAccess")
public class UserAccess {

    /**
     * This is a sample web service operation to do validation.
     */
    @WebMethod(operationName = "validate")
    public int validate(@WebParam(name = "database") String database, @WebParam(name = "username") String username, @WebParam(name = "password") String password) {
        int codeType = 0;
        try {
            codeType = Integer.parseInt(UserAccessDAO.getUserTypeCode(database, username, password));
        } catch (Exception e) {
            System.out.println("Error: " + e);
        }
        return codeType;
    }

    /**
     * This is a sample web service operation
     */
    @WebMethod(operationName = "getHashPassword")
    public String getHashPassword(@WebParam(name = "passwordIn") String passwordIn) {
        return Utility.getHashPassword(passwordIn);
    }
    //Will add the code to add the user to the database here, using the DAO this will allow others to user the web service.
    @WebMethod(operationName = "addUserToDatabase")
    public String addUserToDatabase(@WebParam(name="userToAdd") Users userToAdd, @WebParam(name="databaseConnection") String connection){
        String results = "";
        try{
        results = UserAccessDAO.insert(connection, userToAdd);
        }catch(Exception e){
            System.out.println(e);
        }
        return results ;
    }
    
    @WebMethod(operationName = "updateUser")
    public String updateUser(@WebParam(name="userToUpdate") Users userToUpdate, @WebParam(name="databaseConnection") DatabaseConnection connection){
        try{
            UserAccessDAO.update(connection, userToUpdate);
        }catch(Exception e){
            System.out.println(e);
        }
        return "User updated";
    }
    
    @WebMethod(operationName = "removeUser")
    public String removeUser(@WebParam(name="userToRemove") int userId, @WebParam(name="databaseConnection") DatabaseConnection connection){
        try{
            UserAccessDAO.delete(connection, userId);
        }catch(Exception e){
            System.out.println(e);
        }
        return "User removed";
    }
    
    @WebMethod(operationName = "selectUser")
    public Users selectUser(@WebParam(name="user") int user, @WebParam(name="databaseConnection") DatabaseConnection connection){
        Users userSelect = new Users();
        try{
            userSelect = UserAccessDAO.selectUser(connection, user);
        }catch(Exception e){
            System.out.println(e);
        }
        return userSelect;
    }
}
