package info.hccis.admin.web;

import info.hccis.admin.data.springdatajpa.InfractionRepository;
import info.hccis.admin.data.springdatajpa.PassesRepository;
import info.hccis.admin.model.jpa.Infractions;
import info.hccis.admin.model.jpa.Passes;
import info.hccis.admin.model.jpa.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import java.util.List;

/**
 * Created by compa on 2015-12-17.
 */
@Controller
public class AdminController {
    private final PassesRepository pr;
    private final InfractionRepository ir;

    @Autowired
    public AdminController(PassesRepository parkRepo, InfractionRepository infRepo) {
        this.ir = infRepo;
        this.pr = parkRepo;
    }

    @RequestMapping("/admin/index")
    public String showAdmin(Model model, HttpServletRequest request, HttpSession session) {
        try {
            HttpSession sessionData = request.getSession();

            if (sessionData.getAttribute("userData") != null) {
                User userData = (User) sessionData.getAttribute("userData");
                if (userData.getUserTypeCode() == 12) {
                    List<Passes> passes = pr.findAll();
                    model.addAttribute("passes", passes);
                    return "admin/index";
                }
                return "usertools/permissionError";
            } else {
                return "usertools/pleaseLogin";
            }
        } catch (Exception ex){
            model.addAttribute("message", ex.getMessage());
            return "other/error";
        }
    }

    @RequestMapping("/admin/delete")
    public String deletePass(Model model, HttpServletRequest request, HttpSession session, @RequestParam("passid") Integer passId) {
        try {
            HttpSession sessionData = request.getSession();

            if (sessionData.getAttribute("userData") != null) {
                User userData = (User) sessionData.getAttribute("userData");
                if (userData.getUserTypeCode() == 12) {
                    pr.delete(passId);
                    List<Infractions> inf = ir.findByPassId(passId);

                    for (Infractions infraction : inf) {
                        ir.delete(infraction.getId());
                    }
                    model.addAttribute("title", "Deleted Parking Pass");
                    model.addAttribute("message", "Parking pass was successfully deleted!");
                    return "/other/generalMessage";
                }
                return "usertools/permissionError";
            } else {
                return "usertools/pleaseLogin";
            }
        } catch (Exception ex){
            model.addAttribute("message", ex.getMessage());
            return "other/error";
        }
    }

    @RequestMapping("/admin/addInfraction")
    public String addInfraction(Model model, HttpServletRequest request, HttpSession session, @RequestParam("passid") Integer passId) {
        try {
            HttpSession sessionData = request.getSession();

            if (sessionData.getAttribute("userData") != null) {
                User userData = (User) sessionData.getAttribute("userData");
                if (userData.getUserTypeCode() == 12) {
                    Passes pass = pr.findOne(passId);
                    Infractions inf = new Infractions();
                    java.util.Date date = new java.util.Date();

                    inf.setPassId(passId);
                    inf.setUserId(pass.getHolderId());
                    inf.setInfDate(date.toString());

                    model.addAttribute("inf", inf);
                    return "admin/addInfraction";
                }
                return "usertools/permissionError";
            } else {
                return "usertools/pleaseLogin";
            }
        } catch (Exception ex){
            model.addAttribute("message", ex.getMessage());
            return "other/error";
        }
    }

    @RequestMapping(value = "/admin/processInfraction", method = RequestMethod.POST)
    public String processPassApplication(Model model, HttpServletRequest request, HttpSession session, @Valid @ModelAttribute("inf") Infractions inf) {
        try {
            HttpSession sessionData = request.getSession();

            if (sessionData.getAttribute("userData") != null) {
                User userData = (User) sessionData.getAttribute("userData");
                if (userData.getUserTypeCode() == 12) {
                    ir.save(inf);
                    model.addAttribute("title", "Infraction Added");
                    model.addAttribute("message", "Infraction was successfully added!");
                    return "/other/generalMessage";
                }
                return "usertools/permissionError";
            } else {
                return "usertools/pleaseLogin";
            }
        } catch (Exception ex){
            model.addAttribute("message", ex.getMessage());
            return "other/error";
        }
    }

    @RequestMapping("/admin/viewInfractionsOnPass")
    public String viewInfractionsOnPass(Model model, HttpServletRequest request, HttpSession session, @RequestParam("passid") Integer passId) {
        try {
            HttpSession sessionData = request.getSession();

            if (sessionData.getAttribute("userData") != null) {
                User userData = (User) sessionData.getAttribute("userData");
                if (userData.getUserTypeCode() == 12) {
                    List<Infractions> inf = ir.findByPassId(passId);
                    Passes pass = pr.findOne(passId);

                    model.addAttribute("inf", inf);
                    model.addAttribute("pass", pass);
                    return "admin/viewInfractionsOnPass";
                }
                return "usertools/permissionError";
            } else {
                return "usertools/pleaseLogin";
            }
        } catch (Exception ex){
            model.addAttribute("message", ex.getMessage());
            return "other/error";
        }
    }

    @RequestMapping("/admin/removeInfraction")
    public String removeInfraction(Model model, HttpServletRequest request, HttpSession session, @RequestParam("passid") Integer passId, @RequestParam("infid") Integer infId) {
        try {
            HttpSession sessionData = request.getSession();

            if (sessionData.getAttribute("userData") != null) {
                ir.delete(infId);
                return "redirect:/admin/viewInfractionsOnPass?passid=" + passId;
            } else {
                return "usertools/pleaseLogin";
            }
        } catch (Exception ex){
            model.addAttribute("message", ex.getMessage());
            return "other/error";
        }
    }
}
