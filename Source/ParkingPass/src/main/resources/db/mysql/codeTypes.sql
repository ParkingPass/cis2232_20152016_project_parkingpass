-- phpMyAdmin SQL Dump
-- version 4.5.0.2
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Dec 18, 2015 at 08:20 PM
-- Server version: 10.0.17-MariaDB
-- PHP Version: 5.6.14

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `codes`
--

-- --------------------------------------------------------

--
-- Table structure for table `codetype`
--

CREATE TABLE `codetype` (
  `CodeTypeId` int(3) NOT NULL COMMENT 'This is the primary key for code types',
  `englishDescription` varchar(100) NOT NULL COMMENT 'English description',
  `frenchDescription` varchar(100) DEFAULT NULL COMMENT 'French description',
  `createdDateTime` datetime DEFAULT NULL,
  `createdUserId` varchar(20) DEFAULT NULL,
  `updatedDateTime` datetime DEFAULT NULL,
  `updatedUserId` varchar(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='This hold the code types that are available for the applicat';

--
-- Dumping data for table `codetype`
--

INSERT INTO `codetype` (`CodeTypeId`, `englishDescription`, `frenchDescription`, `createdDateTime`, `createdUserId`, `updatedDateTime`, `updatedUserId`) VALUES
(1, 'User Types', 'User Types FR', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00', ''),
(11, 'Regular System User', 'SystÃ¨me rÃ©gulier utilisateur', '2015-12-17 00:00:00', '1', '2015-12-17 00:00:00', ''),
(12, 'Administrator System User', 'Administrateur systÃ¨me utilisateur', '2015-12-17 00:00:00', '1', '2015-12-17 00:00:00', '');

-- --------------------------------------------------------

--
-- Table structure for table `codevalue`
--

CREATE TABLE `codevalue` (
  `codeTypeId` int(3) NOT NULL COMMENT 'see code_type table',
  `codeValueSequence` int(3) NOT NULL,
  `englishDescription` varchar(100) NOT NULL COMMENT 'English description',
  `englishDescriptionShort` varchar(20) NOT NULL COMMENT 'English abbreviation for description',
  `frenchDescription` varchar(100) DEFAULT NULL COMMENT 'French description',
  `frenchDescriptionShort` varchar(20) DEFAULT NULL COMMENT 'French abbreviation for description',
  `createdDateTime` datetime DEFAULT NULL,
  `createdUserId` varchar(20) DEFAULT NULL,
  `updatedDateTime` datetime DEFAULT NULL,
  `updatedUserId` varchar(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='This will hold code values for the application.';

--
-- Dumping data for table `codevalue`
--

INSERT INTO `codevalue` (`codeTypeId`, `codeValueSequence`, `englishDescription`, `englishDescriptionShort`, `frenchDescription`, `frenchDescriptionShort`, `createdDateTime`, `createdUserId`, `updatedDateTime`, `updatedUserId`) VALUES
(1, 1, 'General', 'General', 'GeneralFR', 'GeneralFR', '2015-10-25 18:44:37', 'admin', '2015-10-25 18:44:37', 'admin'),
(11, 11, 'Regular User', 'Regular User', 'Regular User FR', 'Regular User FR', '2015-12-17 00:00:00', 'admin', '0000-00-00 00:00:00', 'admin'),
(12, 12, 'Admin User', 'Admin User', 'Admin User FR', 'Admin User FR', '0000-00-00 00:00:00', 'admin', '0000-00-00 00:00:00', 'admin');

-- --------------------------------------------------------

--
-- Table structure for table `infractions`
--

CREATE TABLE `infractions` (
  `id` int(11) NOT NULL,
  `pass_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `inf_title` varchar(150) NOT NULL,
  `inf_description` text NOT NULL,
  `inf_date` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `passes`
--

CREATE TABLE `passes` (
  `id` int(11) NOT NULL,
  `holder_id` int(11) NOT NULL,
  `plate_number` varchar(10) NOT NULL,
  `plate_province` varchar(2) NOT NULL,
  `car_make` varchar(25) NOT NULL,
  `car_model` varchar(25) NOT NULL,
  `graduation_year` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `passes`
--

INSERT INTO `passes` (`id`, `holder_id`, `plate_number`, `plate_province`, `car_make`, `car_model`, `graduation_year`) VALUES
(15, 1, 'asdasd', 'NS', 'asdasd', 'asd', 2015);

-- --------------------------------------------------------

--
-- Table structure for table `useraccess`
--

CREATE TABLE `useraccess` (
  `userAccessId` int(3) NOT NULL,
  `username` varchar(100) NOT NULL COMMENT 'Unique user name for app',
  `password` varchar(128) NOT NULL,
  `userTypeCode` int(3) NOT NULL DEFAULT '1' COMMENT 'Code type #1',
  `createdDateTime` datetime DEFAULT NULL COMMENT 'When user was created.'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `useraccess`
--

INSERT INTO `useraccess` (`userAccessId`, `username`, `password`, `userTypeCode`, `createdDateTime`) VALUES
(1, 'skyllama', 'd07708e7595eb0f0bab955be2c3ad7e', 12, '2015-12-14 00:00:00'),
(7, 'admin', '192023a7bbd73250516f069df18b500', 12, '2015-12-18 10:14:58'),
(8, 'testuser', 'e99a18c428cb38d5f260853678922e03', 11, '2015-12-18 12:52:06');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `codetype`
--
ALTER TABLE `codetype`
  ADD PRIMARY KEY (`CodeTypeId`);

--
-- Indexes for table `codevalue`
--
ALTER TABLE `codevalue`
  ADD PRIMARY KEY (`codeValueSequence`),
  ADD KEY `codeTypeId` (`codeTypeId`);

--
-- Indexes for table `infractions`
--
ALTER TABLE `infractions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `passes`
--
ALTER TABLE `passes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `useraccess`
--
ALTER TABLE `useraccess`
  ADD PRIMARY KEY (`userAccessId`),
  ADD KEY `userTypeCode` (`userTypeCode`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `codetype`
--
ALTER TABLE `codetype`
  MODIFY `CodeTypeId` int(3) NOT NULL AUTO_INCREMENT COMMENT 'This is the primary key for code types', AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT for table `codevalue`
--
ALTER TABLE `codevalue`
  MODIFY `codeValueSequence` int(3) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT for table `infractions`
--
ALTER TABLE `infractions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `passes`
--
ALTER TABLE `passes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;
--
-- AUTO_INCREMENT for table `useraccess`
--
ALTER TABLE `useraccess`
  MODIFY `userAccessId` int(3) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `codevalue`
--
ALTER TABLE `codevalue`
  ADD CONSTRAINT `codevalue_ibfk_1` FOREIGN KEY (`codeTypeId`) REFERENCES `codetype` (`CodeTypeId`);

--
-- Constraints for table `useraccess`
--
ALTER TABLE `useraccess`
  ADD CONSTRAINT `useraccess_ibfk_1` FOREIGN KEY (`userTypeCode`) REFERENCES `codevalue` (`codeValueSequence`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
