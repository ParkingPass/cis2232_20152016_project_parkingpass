package info.hccis.admin.data.springdatajpa;

import info.hccis.admin.model.jpa.Passes;
import info.hccis.admin.model.jpa.User;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by compa on 2015-12-13.
 */

@Repository
public interface PassesRepository extends CrudRepository<Passes, Integer> {
    Passes findOne(Integer id);
    List<Passes> findAll();
    List<Passes> findByHolderId(Integer holderId);

}
