package info.hccis.admin.util;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.security.*;
import java.math.*;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * General program utilities
 * @author bjmaclean
 * @since 20150918
 */
public class Utility {
    private static Scanner input = new Scanner(System.in);

    public static Scanner getInput() {
        return input;
    }
    
    public static String getHashPassword(String inPassword){
        try {
            return getMD5Hash(inPassword);
        } catch (Exception ex) {
            Logger.getLogger(Utility.class.getName()).log(Level.SEVERE, null, ex);
        }
        return "Should not see this!";
    }
    

    public static String getMD5Hash(String passwordIn) throws Exception{
        MessageDigest m=MessageDigest.getInstance("MD5");
        m.update(passwordIn.getBytes(),0,passwordIn.length());
        return ""+new BigInteger(1,m.digest()).toString(16);
    }

    public static String getResponseFromRest(String urlString){
        try {

            URL url = new URL(urlString);

            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setRequestMethod("GET");
            conn.setRequestProperty("Accept", "application/json");

            if (conn.getResponseCode() != 200) {
                throw new RuntimeException("Failed : HTTP error code : "
                        + conn.getResponseCode());
            }

            BufferedReader br = new BufferedReader(new InputStreamReader(
                    (conn.getInputStream())));

            String output;
            String theOutput = "";
            System.out.println("Output from Server .... \n");
            while ((output = br.readLine()) != null) {
                theOutput += output;
                System.out.println(output);
            }

            conn.disconnect();

//            ObjectMapper om = new ObjectMapper();
//            Address address = om.readValue(theOutput, Address.class);
            return theOutput;
            //JOptionPane.showMessageDialog(null, theOutput);
//            JOptionPane.showMessageDialog(null, address.toString());
        } catch (MalformedURLException e) {

            e.printStackTrace();

        } catch (IOException e) {

            e.printStackTrace();

        }
        return null;
    }
}
    
    

