package info.hccis.admin.dao;

import info.hccis.admin.model.*;
import info.hccis.admin.dao.util.ConnectionUtils;
import info.hccis.admin.dao.util.DbUtils;
import info.hccis.admin.model.jpa.Passes;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.time.Year;
import java.util.ArrayList;
import java.util.Calendar;

/**
 * Created by compa on 2015-12-13.
 */
public class PassAccessDAO {
    /**
     * Get all of the code types.
     * @return List of passes
     * @since 20151015
     * @author BJ MacLean
     */
    public static ArrayList<Passes> getPasses(DatabaseConnection databaseConnection) {
        ArrayList<Passes> passes = new ArrayList();

        PreparedStatement ps = null;
        String sql = null;
        Connection conn = null;

        try {
            conn = ConnectionUtils.getConnection(databaseConnection);

            sql = "SELECT * FROM passes" ;

            ps = conn.prepareStatement(sql);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                // It is possible to get the columns via name
                // also possible to get the columns via the column number
                // which starts at 1

                // e.g. resultSet.getString(2);

                Passes aClass = new Passes();
                aClass.setId(rs.getInt("id"));
                aClass.setHolderId(rs.getInt("holder_id"));
                aClass.setCarMake(rs.getString("car_make"));
                aClass.setCarModel(rs.getString("car_model"));
                aClass.setPlateNumber(rs.getString("plate_number"));
                aClass.setPlateProvince(rs.getString("plate_province"));
                aClass.setGraduationYear(rs.getInt("graduation_year"));

                System.out.println("Found pass type=" + aClass);
                passes.add(aClass);
            }
        } catch (Exception e) {
            String errorMessage = e.getMessage();
            e.printStackTrace();
        } finally {
            DbUtils.close(ps, conn);
        }

        return passes;

    }

    /*public static void add(DatabaseConnection databaseConnection, Assignment classToAdd) {
        PreparedStatement ps = null;
        String sql = null;
        Connection conn = null;

        try {
            conn = ConnectionUtils.getConnection(databaseConnection);

            sql = "INSERT INTO assignments (id, "
                    + "assignmentName, assignmentWeight)"
                    + " VALUES (NULL, '"  + classToAdd.getAssignmentName()+ "', " + classToAdd.getAssignmentWeight() + ")";
            System.out.println("\n\n\n" + sql.toString());

            ps = conn.prepareStatement(sql);
            ps.executeUpdate();

        } catch (SQLException ex) {
            Logger.getLogger(assignmentsDAO.class.getName()).log(Level.SEVERE, null, ex);
        } catch (Exception e) {
            String errorMessage = e.getMessage();
            e.printStackTrace();
        } finally {
            DbUtils.close(ps, conn);
        }
    }*/
}
