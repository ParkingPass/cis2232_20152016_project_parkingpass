/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package info.hccis.admin.model.entity;

/**
 *
 * @author knewcombe
 */
public class Users {
    private int userAccessId = 0;
    private String username = "";
    private int userTypeCode = 0;
    private String userTypeDescription = "";
    private String createdDateTime = "";
    private String password = "";
    
    public Users(){
    
    }
    
    public Users(int userAccessId, String username, int userTypeCode, String createdDateTime, String password){
        this.userAccessId = userAccessId;
        this.username = username;
        this.userTypeCode = userTypeCode;
        this.createdDateTime = createdDateTime;
        this.password = password;
    }

    public int getUserAccessId() {
        return userAccessId;
    }

    public void setUserAccessId(int userAccessId) {
        this.userAccessId = userAccessId;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public int getUserTypeCode() {
        return userTypeCode;
    }

    public void setUserTypeCode(int userTypeCode) {
        this.userTypeCode = userTypeCode;
        
    }

    public String getCreatedDateTime() {
        return createdDateTime;
    }

    public void setCreatedDateTime(String createdDateTime) {
        this.createdDateTime = createdDateTime;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getUserTypeDescription() {
        return userTypeDescription;
    }

    public void setUserTypeDescription(String userTypeDescription) {
        this.userTypeDescription = userTypeDescription;
    }

}
