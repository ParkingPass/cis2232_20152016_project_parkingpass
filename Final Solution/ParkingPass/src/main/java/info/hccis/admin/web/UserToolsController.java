package info.hccis.admin.web;

import info.hccis.admin.data.springdatajpa.*;
import info.hccis.admin.model.UserAccess;
import info.hccis.admin.model.jpa.User;
import info.hccis.admin.util.Utility;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

/**
 * Created by compa on 2015-12-17.
 */
@Controller
public class UserToolsController {
    private final UsersRepository usr;

    @Autowired
    public UserToolsController(UsersRepository usr){
        this.usr = usr;
    }

    @RequestMapping("/login/loginForm")
    public String showLogin(Model model, HttpServletRequest request, HttpSession session) {
        try {
            HttpSession sessionData = request.getSession();

            if (sessionData.getAttribute("userData") != null) {
                sessionData.removeAttribute("userData");
                return "redirect:/";
            } else {
                UserAccess login = new UserAccess();
                model.addAttribute("login", login);
                model.addAttribute("message", "");
                return "usertools/login";
            }
        } catch (Exception ex){
            model.addAttribute("message", ex.getMessage());
            return "other/error";
        }
    }

    @RequestMapping(value="/login/loginProcess",method= RequestMethod.POST)
    public String processLogin(Model model, HttpSession session, @ModelAttribute("login") UserAccess login) {
        try {
            String urlString = "http://localhost:8080/parkingpass/rest/useraccess/codes," + login.getUsername() + "," + login.getPassword();
            String result = Utility.getResponseFromRest(urlString);

            if (result.equals("11") || result.equals("12")) {
                User userData = usr.findOneByUserName(login.getUsername());
                login.setUserTypeCode(Integer.parseInt(result));
                session.setAttribute("userData", userData);
                return "redirect:/";
            } else {
                model.addAttribute("message", "Login failed");
                model.addAttribute("login", login);
                return "usertools/login";
            }
        } catch (Exception ex){
            model.addAttribute("message", ex.getMessage());
            return "other/error";
        }
    }

    @RequestMapping("/login/userRegister")
    public String userRegister(Model model, HttpServletRequest request, HttpSession session) {
        try {
            HttpSession sessionData = request.getSession();

            if (sessionData.getAttribute("userData") != null) {
                return "redirect:/";
            } else {
                User user = new User();
                user.setUserTypeCode(11);
                model.addAttribute("user", user);
                return "usertools/userRegister";
            }
        }catch (Exception ex){
            model.addAttribute("message", ex.getMessage());
            return "other/error";
        }
    }

    @RequestMapping(value="/login/registerProcess",method= RequestMethod.POST)
    public String registerProcess(Model model, HttpSession session, @ModelAttribute("user") User user) {
        try {
            String urlString = "http://localhost:8080/parkingpass/rest/useraccess/add/codes," + user.getUserName() + "," + user.getPassWord() + "," + user.getUserTypeCode();
            System.out.println("\n\n" + urlString + "\n\n");
            String result = Utility.getResponseFromRest(urlString);

            if (!result.equals("0")) {
                return "redirect:/";
            } else {
                model.addAttribute("user", user);
                return "redirect:/login/userRegister";
            }
        } catch (Exception ex){
            model.addAttribute("message", ex.getMessage());
            return "other/error";
        }
    }
}
