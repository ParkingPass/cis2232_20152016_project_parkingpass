package info.hccis.admin.model.jpa;

import java.time.Year;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Entity
@Table(name = "passes")
public class Passes {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;

    @Basic(optional = false)
    @Column(name = "holder_id")
    private Integer holderId;

    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 10)
    @Column(name = "plate_number")
    private String plateNumber;

    @Basic(optional = false)
    @NotNull
    @Size(min = 2, max = 2)
    @Column(name = "plate_province")
    private String plateProvince;

    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 25)
    @Column(name = "car_make")
    private String carMake;

    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 25)
    @Column(name = "car_model")
    private String carModel;

    @Basic(optional = false)
    @Column(name = "graduation_year")
    private Integer graduationYear;


    public Passes() {
    }

    public Passes(Integer passID) {
        this.id = passID;
    }

    public Integer getId() {
        return id;
    }
    public void setId(Integer passID) {
        this.id = passID;
    }

    public Integer getHolderId() {
        return holderId;
    }
    public void setHolderId(Integer num) {
        this.holderId = num;
    }

    public String getPlateNumber() {
        return plateNumber;
    }
    public void setPlateNumber(String num) {
        this.plateNumber = num;
    }

    public String getPlateProvince() {
        return plateProvince;
    }
    public void setPlateProvince(String pro) {
        this.plateProvince = pro;
    }

    public String getCarMake() {
        return carMake;
    }
    public void setCarMake(String make) {
        this.carMake = make;
    }

    public String getCarModel() {
        return carModel;
    }
    public void setCarModel(String model) { this.carModel = model; }

    public Integer getGraduationYear() {
        return graduationYear;
    }
    public void setGraduationYear(Integer year) { this.graduationYear = year; }

}
