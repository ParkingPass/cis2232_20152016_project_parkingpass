There are two types a user can be, type 11 and 12 which are represented by the code types.
    -Type 11 is a regular user,
    -Type 12 is an administrator.
These codes for the user are supper important as the program will only recognise them as valid. Without on of these
a user account will never be able to login.

The registration system uses a modified version of the UserAccess Rest web service. I could not figure out how
to use it as is, so I had to compromise in order to actually get this done in time. Depending on how these are deployed
it may have to be edited, but the program will happily contact itself to access this modified web service.

The URL for the login service might need to be changed after being deployed. This is actually very likely.

Demo can be seen here: https://www.youtube.com/watch?v=kPphsyP1ZgA
Bitbucket Repo: https://bitbucket.org/ParkingPass/cis2232_20152016_project_parkingpass
Repo on my git server:

